from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from receipts.models import Receipt, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, CreateAccountForm

# Create your views here.


@login_required
def list_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list_receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    # put form in the context
    context = {
        "form": form,
    }
    # render the HTML template with the form
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    categories = {}
    for rec in receipts:
        categories[rec.category] = categories.get(rec.category, 0) + 1
    context = {
        "categories": categories,
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    accounts = Account.objects.filter(owner=request.user)
    items = {acc.name: {'number': acc.number,
                        'count': len(receipts.filter(account=acc)),
                        } for acc in accounts}
    context = {
        "accounts": items,
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    # put form in the context
    context = {
        "form": form,
    }
    # render the HTML template with the form
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    # put form in the context
    context = {
        "form": form,
    }
    # render the HTML template with the form
    return render(request, "receipts/create_account.html", context)
